from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.books, name='books'),
    path('data/', views.data, name='data'),
]