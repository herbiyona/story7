from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from .views import books
from .apps import Story8Config
from django.http import JsonResponse
import json

# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.books = reverse('story8:books')

    def test_landing_page(self):
        found = resolve(self.books)
        self.assertEqual(found.func, books)

class ViewsTest(TestCase):
    def setUp(self):
        self.books = reverse("story8:books")

    def test_books_get(self):
        response = Client().get(self.books, {'q':'Matematika'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books.html')
    
    def test_jsonresponse_success(self):
        response = self.client.get("/story8/data?q=Resep")
        self.assertEqual(response.status_code, 301)

class AppTest(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')