from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def books(request):
    return render(request, 'books.html')

def data(request):
    getdata = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q'])
    jsondata = json.loads(getdata.content)
    return JsonResponse(jsondata, safe=False)
