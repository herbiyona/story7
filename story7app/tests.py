from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from .views import activities
from .apps import Story7AppConfig

# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.activities = reverse('story7app:activities')

    def test_landing_page(self):
        found = resolve(self.activities)
        self.assertEqual(found.func, activities)

class ViewsTest(TestCase):
    def setUp(self):
        self.activities = reverse("story7app:activities")

    def test_allactivities_get(self):
        response = Client().get(self.activities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activities.html')

class AppTest(TestCase):
    def test_app(self):
        self.assertEqual(Story7AppConfig.name, 'story7app')