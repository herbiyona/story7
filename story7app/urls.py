from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story7app'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.activities, name='activities'),
]