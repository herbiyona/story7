from django.test import TestCase, Client
from django.urls import resolve,reverse
from django.apps import apps
from .views import home, signup, signin, signout
from .apps import Story9Config
from django.contrib.auth.models import User

# Create your tests here.

class FormTest(TestCase):
    def setUp(self):
        self.akun = User.objects.create_user(username="pwuj",email="pwuj@yahoo.com",password="yukstoryterakhiryuk")

    def test_user_created(self):
        self.assertEqual(User.objects.all().count(),1)

class UrlsTest(TestCase):
    def setUp(self):
        self.home = reverse('story9:home')
        # self.signin = reverse('story9:signin')
        self.signup = reverse('story9:signup')

    def test_landing_page(self):
        found = resolve(self.home)
        self.assertEqual(found.func, home)
    

class ViewsTest(TestCase):
    def setUp(self):
        self.akun = User.objects.create_user(username="pwuj",email="pwuj@yahoo.com",password="yukstoryterakhiryuk")
        self.home = reverse('story9:home')
        self.signin = reverse('story9:signin')
        self.signup = reverse('story9:signup')
        self.signout = reverse('story9:signout')

    def test_home(self):
        response = Client().get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')
    
    def test_signup_fail(self):
        response = Client().post(self.signup,{'username':"pwuj", 'email':'pwuj@gmail.com',"password":'halo'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_success(self):
        response = Client().post(self.signup, {'username':'u', 'email':'yuyu@gmail.com','password':'hayuu'})
        self.assertEqual(response.status_code, 302)

    def test_login_fail_wrongpass(self):
        response = Client().post(self.signin, {'username':"pwuj", "password":'haihaihai'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_login_fail_nousername(self):
        response = Client().post(self.signin, {'username':"pepewe", "password":'haihaihai'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_login_success(self):
        response = Client().post(self.signin, {'username':'pwuj', 'password':"yukstoryterakhiryuk"})
        self.assertEqual(response.status_code, 302)

    def test_logout(self):
        response = Client().get(self.signout)
        self.assertEqual(response.status_code, 302)

class AppTest(TestCase):
    def test_app(self):
        self.assertEqual(Story9Config.name, 'story9')