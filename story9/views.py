from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.
def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        if User.objects.filter(username=username).exists():
            messages.error(request, "username is already taken, please use another name")
        else:
            user = User.objects.create_user(username,email,password)
            return redirect ('/story9/signin/')
    return render(request, 'signup.html')

def signin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if User.objects.filter(username=username).exists():
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/story9/')
            else:
                messages.error(request, "Invalid password, please check it again")
        else:
            messages.error(request, "username is not exist, please check or sign up an account")
    return render(request, 'login.html')

def signout(request):
    logout(request)
    return redirect('/story9/')
