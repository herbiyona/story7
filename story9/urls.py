from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('signup/', views.signup, name='signup'),
    path('signin/', views.signin, name='signin'),
    path('signout/', views.signout, name='signout'),
]