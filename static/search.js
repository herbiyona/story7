// async search
$(document).ready(function() {
    $("#search").on("keyup", function() {
        var q = $("#search").val();
        $.ajax({
            url: 'data?q=' + q,
            success: function(data) {
                $('#hasil').html('') //memanggil HTML
                var res = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    res += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>"
                }
                $("#hasil").append(res);
            }
        })
    });
});

// default saat pertama kali buka
$( window ).on( "load", function() { 
    var q = "Resep"
    $.ajax({
        url: 'data?q=' + q,
        success: function(data) {
            $('#hasil').html('')
            var res = '<tr>';
            for (var i = 0; i < data.items.length; i++) {
                res += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                    "<td><img class='img-fluid' style='width:22vh' src='" +
                    data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>"
            }
            $("#hasil").append(res);
        }
    })
});