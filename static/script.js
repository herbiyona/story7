$(document).ready(function() {
  $( "#aktivitas" ).accordion({
    collapsible: true,
    active: false,
    heightStyle: "content",
  });
  $( "#kepanitiaan" ).accordion({
    collapsible: true,
    active: false,
    heightStyle: "content",
  });
  $( "#prestasi" ).accordion({
    collapsible: true,
    active: false,
    heightStyle: "content",
  });
  $( "#hobi" ).accordion({
    collapsible: true,
    active: false,
    heightStyle: "content",
  });
  $( ".upbutton" ).click(function(){//this itu si button
    var naik = $(this).parent().parent().parent();///merujuk ke accordion id aktivitas
    naik.insertBefore(naik.prev());
  });
  $( ".downbutton" ).click(function(){
    var turun = $(this).parent().parent().parent();
    turun.insertAfter(turun.next());
  });
});